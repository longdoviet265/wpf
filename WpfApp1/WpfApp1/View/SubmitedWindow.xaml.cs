﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for SubmitedWindow.xaml
    /// </summary>
    public partial class SubmitedWindow : Window
    {
        public SubmitedWindow()
        {
            InitializeComponent();
        }
        public SubmitedWindow(Exam eaxm) : this()
        {
            ExamView examView = new ExamView()
            {
                name = eaxm.Name,
                isExpired = eaxm.IsExpired
            };
            this.DataContext = examView;
        }
        private class ExamView
        {
            public string name { get; set; }
            public bool isExpired { get; set; }
            public int anwseredNum { get; set; } = 48;
            public int anwserTotal { get; set; } = 56;
        }
        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            ExamList elw = new ExamList();
            this.Close();
            elw.Show();
        }
    }
}
