﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using WpfApp1.Db;
using WpfApp1.Model;
using WpfApp1.View;
using static WpfApp1.ExamWindow;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for ExamWindow.xaml
    /// </summary>
    public partial class ExamWindow : Window, INotifyPropertyChanged
    {
        public int currentId = 1;
        public DispatcherTimer _timer;
        public int countReviewLater;
        LocalDb localDb = new LocalDb();

        public event PropertyChangedEventHandler? PropertyChanged;

        public enum QuestionType : int
        {
            Choice = 1,
            FillInTheBlank = 2,
            SelectBox = 3,
            ShortAnwser = 4,
            Ranking = 5,
            DragDrop = 6,
            Essay = 7,
            Matching = 8,
            MultipleChoice = 9
        }
        public ObservableCollection<Question> CreateQuestions()
        {
            ObservableCollection<Question> questions = new ObservableCollection<Question>();
            questions.Add(new Question() { Id = 1, Module = "Module A", Type = 1 });
            questions.Add(new Question() { Id = 2, Module = "Module B", Type = 2 });
            questions.Add(new Question() { Id = 3, Module = "Module A", Type = 3 });
            questions.Add(new Question() { Id = 4, Module = "Module D", Type = 4 });
            questions.Add(new Question() { Id = 5, Module = "Module A", Type = 8 });
            questions.Add(new Question() { Id = 6, Module = "Module A", Type = 9 });
            return questions;
        }
        public ObservableCollection<MatchingQuestion> CreateMatchingContent()
        {
            ObservableCollection<MatchingQuestion> questions = new ObservableCollection<MatchingQuestion>();
            questions.Add(new MatchingQuestion() { Prompt = "China" });
            questions.Add(new MatchingQuestion() { Prompt = "India" });
            questions.Add(new MatchingQuestion() { Prompt = "United State" });
            questions.Add(new MatchingQuestion() { Prompt = "Indonesia" });
            return questions;
        }
        public ExamWindow()
        {
            InitializeComponent();
            #region CountDown
            //Countdown
            _timer = new DispatcherTimer();
            _timer.Interval = new TimeSpan(0, 0, 1);
            _timer.Tick += Timer_Tick;
            _timer.Start();
            #endregion
            var questions = CreateQuestions();
            //ShowQuestion(questions[0].Type);
            //questions[0].isClick = true;
            //questions[0].isCurrent = true;
            this.DataContext = questions;
            moduleData.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = questions });
            //Group ItemsControl by Property
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(moduleData.ItemsSource);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("Module");
            view.GroupDescriptions.Add(groupDescription);
            //
            var matchingContent = CreateMatchingContent();
            questionMatching.ItemsSource = matchingContent;
        }
        #region CountDown Event
        private void Timer_Tick(object sender, EventArgs e)
        {
            ExamDb examDb = GetExamLocal();
            DateTime timeEnd = examDb.TimeFinish > examDb.EstimateEndTime ? examDb.EstimateEndTime : examDb.TimeFinish;
            TimeSpan t = (timeEnd - DateTime.UtcNow);
            if (t.TotalSeconds > 0)
            {
                string countDown = string.Format("{0:00}:{1:00}:{2:00}", t.Hours, t.Minutes, t.Seconds);
                txblCountDown.Text = countDown;
            }
            else
            {
                _timer.Stop();
                ExpiredWindow ew = new ExpiredWindow();
                this.Close();
                ew.Show();
                if (App.Current.Windows.Count > 1)
                {
                    CloseAllWindows();
                }
            }
        }
        #endregion
        private void btnExam_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            var question = button.DataContext as Question;
            if (question != null)
            {
                foreach (Question item in moduleData.Items)
                {
                    if (item.Id != question.Id)
                    {
                        item.isCurrent = false;

                    }
                }
                question.isClick = true;
                question.isCurrent = true;
                currentId = question.Id;
                ShowQuestion(question.Type);
                this.DataContext = question;
            }
        }
        private void ShowQuestion(int type)
        {
            switch (type)
            {
                case (int)QuestionType.Choice:
                    gQ1.Visibility = Visibility.Visible;
                    gQ2.Visibility = Visibility.Collapsed;
                    gQ3.Visibility = Visibility.Collapsed;
                    gQ4.Visibility = Visibility.Collapsed;
                    gQ8.Visibility = Visibility.Collapsed;
                    gQ9.Visibility = Visibility.Collapsed;
                    break;
                case (int)QuestionType.FillInTheBlank:
                    gQ2.Visibility = Visibility.Visible;
                    gQ1.Visibility = Visibility.Collapsed;
                    gQ3.Visibility = Visibility.Collapsed;
                    gQ4.Visibility = Visibility.Collapsed;
                    gQ8.Visibility = Visibility.Collapsed;
                    gQ9.Visibility = Visibility.Collapsed;
                    break;
                case (int)QuestionType.SelectBox:
                    gQ3.Visibility = Visibility.Visible;
                    gQ1.Visibility = Visibility.Collapsed;
                    gQ2.Visibility = Visibility.Collapsed;
                    gQ4.Visibility = Visibility.Collapsed;
                    gQ8.Visibility = Visibility.Collapsed;
                    gQ9.Visibility = Visibility.Collapsed;
                    break;
                case (int)QuestionType.ShortAnwser:
                    gQ4.Visibility = Visibility.Visible;
                    gQ1.Visibility = Visibility.Collapsed;
                    gQ2.Visibility = Visibility.Collapsed;
                    gQ3.Visibility = Visibility.Collapsed;
                    gQ8.Visibility = Visibility.Collapsed;
                    gQ9.Visibility = Visibility.Collapsed;
                    break;
                case (int)QuestionType.Matching:
                    gQ4.Visibility = Visibility.Collapsed;
                    gQ1.Visibility = Visibility.Collapsed;
                    gQ2.Visibility = Visibility.Collapsed;
                    gQ3.Visibility = Visibility.Collapsed;
                    gQ8.Visibility = Visibility.Visible;
                    gQ9.Visibility = Visibility.Collapsed;
                    break;
                case (int)QuestionType.MultipleChoice:
                    gQ4.Visibility = Visibility.Collapsed;
                    gQ1.Visibility = Visibility.Collapsed;
                    gQ2.Visibility = Visibility.Collapsed;
                    gQ3.Visibility = Visibility.Collapsed;
                    gQ8.Visibility = Visibility.Collapsed;
                    gQ9.Visibility = Visibility.Visible;
                    break;
            }
        }
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            var question = moduleData.Items.Cast<Question>().Where(t => t.Id == currentId).First();
            var index = moduleData.Items.IndexOf(question);
        }
        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
        }
        private void btnReview_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            var question = button.DataContext as Question;
            if (question != null)
            {
                question.isReview = !question.isReview;
            }
        }
        private ExamDb GetExamLocal()
        {
            ExamDb examDb = new ExamDb();
            string sql = "SELECT TOP 1 * FROM Exams";
            DataTable dt = localDb.GetTable(sql);
            foreach (DataRow item in dt.Rows)
            {
                examDb.Id = Convert.ToInt32(item["Id"]);
                examDb.ExamId = Convert.ToInt32(item["ExamId"]);
                examDb.TimeStart = Convert.ToDateTime(item["StartTime"]);
                examDb.TimeFinish = Convert.ToDateTime(item["FinishTime"]);
                examDb.EstimateEndTime = Convert.ToDateTime(item["EstimateEndTime"]);
                examDb.StartingTime = Convert.ToDateTime(item["StartingTime"]);

            }
            return examDb;
        }
        private void btnSummary_Click(object sender, RoutedEventArgs e)
        {
            SummaryWindow sm = new SummaryWindow();
            sm.ShowDialog();
        }
        private void CloseAllWindows()
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
            {
                if (intCounter != App.Current.Windows.Count - 1)
                {
                    App.Current.Windows[intCounter].Close();
                }
            }
        }
        private void ZoomButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var a = sender;
            var imageSource = imgQuestion.Source;
            if (imageSource != null)
            {
                Popup myPopup = new Popup(imageSource);
                myPopup.Show();
            }
        }
        //private void rankingControl_Drop(object sender, DragEventArgs e)
        //{
        //    MessageBox.Show("aaaa");

        //}

        //private void rankingControl_DragOver(object sender, DragEventArgs e)
        //{
        //    MessageBox.Show("aaaa");
        //}
    }
}
