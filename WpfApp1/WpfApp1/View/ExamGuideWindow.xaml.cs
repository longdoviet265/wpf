﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Db;
using WpfApp1.Model;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for ExamGuideWindow.xaml
    /// </summary>
    public partial class ExamGuideWindow : Window
    {
        private Exam _exam;
        LocalDb localDb = new LocalDb();
        public ExamGuideWindow()
        {
            InitializeComponent();
        }

        public ExamGuideWindow(Exam passedExam) : this()
        {
            _exam = passedExam;
            this.DataContext = _exam;
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            ExamWindow ew = new ExamWindow();
            var exam = GetExamLocal();
            if (exam != null && !exam.IsStart)
            {
                DateTime now = DateTime.UtcNow;
                DateTime estimateEndTime = now.AddMinutes(10);
                string cmd = "UPDATE Exams SET EstimateEndTime = CAST('" + estimateEndTime + "' AS DATETIME), StartingTime = CAST('" + now + "' AS DATETIME), IsStart = 1 WHERE Id = 1";
                localDb.ExcuteSql(cmd);
            }
           
            this.Close();
            ew.Show();
        }

        private ExamDb GetExamLocal()
        {
            ExamDb examDb = new ExamDb();
            string sql = "SELECT TOP 1 * FROM Exams";
            DataTable dt = localDb.GetTable(sql);
            foreach (DataRow item in dt.Rows)
            {
                examDb.Id = Convert.ToInt32(item["Id"]);
                examDb.ExamId = Convert.ToInt32(item["ExamId"]);
                examDb.TimeStart = Convert.ToDateTime(item["StartTime"]);
                examDb.TimeFinish = Convert.ToDateTime(item["FinishTime"]);
                examDb.EstimateEndTime = Convert.ToDateTime(item["EstimateEndTime"]);
                examDb.StartingTime = Convert.ToDateTime(item["StartingTime"]);
                examDb.IsStart = Convert.ToBoolean(item["IsStart"]);
            }
            return examDb;
        }
    }
}
