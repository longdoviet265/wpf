﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Model;

namespace WpfApp1.View
{
    /// <summary>
    /// Interaction logic for QuestionRemainingWindow.xaml
    /// </summary>
    public partial class QuestionRemainingWindow : Window
    {
        private bool isSortId = false;
        private bool isSortStatus = false;
        public QuestionRemainingWindow()
        {
            InitializeComponent();
            var questions = CreateQuestions();
            questionList.ItemsSource = questions.Where(t => t.isAttempted == false);
            //Group ItemsControl by Property
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(questionList.ItemsSource);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("Module");
            view.GroupDescriptions.Add(groupDescription);
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            btnNotAttempted.Background = new SolidColorBrush(Color.FromRgb(27, 101, 248));
            txtNotAttempted.Foreground = new SolidColorBrush(Color.FromRgb(253, 253, 253));
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        public ObservableCollection<Question> CreateQuestions()
        {
            ObservableCollection<Question> questions = new ObservableCollection<Question>();
            questions.Add(new Question() { Id = 1, Module = "Module A", Type = 1 });
            questions.Add(new Question() { Id = 2, Module = "Module B", Type = 2, isReview = true, isAttempted = true });
            questions.Add(new Question() { Id = 3, Module = "Module A", Type = 3 });
            questions.Add(new Question() { Id = 4, Module = "Module D", Type = 4 });
            questions.Add(new Question() { Id = 5, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 6, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 7, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 8, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 9, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 10, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 11, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 12, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 13, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 14, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 15, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 16, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 17, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 18, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 19, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 20, Module = "Module A", Type = 5 });
            questions.Add(new Question() { Id = 21, Module = "Module A", Type = 5 });
            return questions;
        }
        private void btnAll_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var questions = CreateQuestions();
            btnAll.Background = new SolidColorBrush(Color.FromRgb(27, 101, 248));
            txtAll.Foreground = new SolidColorBrush(Color.FromRgb(253, 253, 253));
            questionList.ItemsSource = questions;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(questionList.ItemsSource);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("Module");
            view.GroupDescriptions.Add(groupDescription);

            btnAttempted.Background = new SolidColorBrush(Color.FromRgb(240, 240, 240));
            txtAttempted.Foreground = new SolidColorBrush(Color.FromRgb(18, 18, 18));

            btnNotAttempted.Background = new SolidColorBrush(Color.FromRgb(240, 240, 240));
            txtNotAttempted.Foreground = new SolidColorBrush(Color.FromRgb(18, 18, 18));

            btnReviewLater.Background = new SolidColorBrush(Color.FromRgb(240, 240, 240));
            txtReviewLater.Foreground = new SolidColorBrush(Color.FromRgb(18, 18, 18));
        }
        private void btnAttempted_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var questions = CreateQuestions();
            btnAttempted.Background = new SolidColorBrush(Color.FromRgb(27, 101, 248));
            txtAttempted.Foreground = new SolidColorBrush(Color.FromRgb(253, 253, 253));
            questionList.ItemsSource = questions.Where(t => t.isAttempted == true);
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(questionList.ItemsSource);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("Module");
            view.GroupDescriptions.Add(groupDescription);

            btnAll.Background = new SolidColorBrush(Color.FromRgb(240, 240, 240));
            txtAll.Foreground = new SolidColorBrush(Color.FromRgb(18, 18, 18));

            btnNotAttempted.Background = new SolidColorBrush(Color.FromRgb(240, 240, 240));
            txtNotAttempted.Foreground = new SolidColorBrush(Color.FromRgb(18, 18, 18));

            btnReviewLater.Background = new SolidColorBrush(Color.FromRgb(240, 240, 240));
            txtReviewLater.Foreground = new SolidColorBrush(Color.FromRgb(18, 18, 18));
        }
        private void btnNotAttempted_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var questions = CreateQuestions();
            btnNotAttempted.Background = new SolidColorBrush(Color.FromRgb(27, 101, 248));
            txtNotAttempted.Foreground = new SolidColorBrush(Color.FromRgb(253, 253, 253));
            questionList.ItemsSource = questions.Where(t => t.isAttempted == false);
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(questionList.ItemsSource);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("Module");
            view.GroupDescriptions.Add(groupDescription);

            btnAll.Background = new SolidColorBrush(Color.FromRgb(240, 240, 240));
            txtAll.Foreground = new SolidColorBrush(Color.FromRgb(18, 18, 18));

            btnAttempted.Background = new SolidColorBrush(Color.FromRgb(240, 240, 240));
            txtAttempted.Foreground = new SolidColorBrush(Color.FromRgb(18, 18, 18));

            btnReviewLater.Background = new SolidColorBrush(Color.FromRgb(240, 240, 240));
            txtReviewLater.Foreground = new SolidColorBrush(Color.FromRgb(18, 18, 18));
        }
        private void btnReviewLater_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var questions = CreateQuestions();
            btnReviewLater.Background = new SolidColorBrush(Color.FromRgb(27, 101, 248));
            txtReviewLater.Foreground = new SolidColorBrush(Color.FromRgb(253, 253, 253));
            questionList.ItemsSource = questions.Where(t => t.isReview == true);
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(questionList.ItemsSource);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("Module");
            view.GroupDescriptions.Add(groupDescription);

            btnAll.Background = new SolidColorBrush(Color.FromRgb(240, 240, 240));
            txtAll.Foreground = new SolidColorBrush(Color.FromRgb(18, 18, 18));

            btnAttempted.Background = new SolidColorBrush(Color.FromRgb(240, 240, 240));
            txtAttempted.Foreground = new SolidColorBrush(Color.FromRgb(18, 18, 18));

            btnNotAttempted.Background = new SolidColorBrush(Color.FromRgb(240, 240, 240));
            txtNotAttempted.Foreground = new SolidColorBrush(Color.FromRgb(18, 18, 18));
        }
        private void btnSortId_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isSortId = !isSortId;
            if (!isSortId)
            {
                questionList.ItemsSource = questionList.ItemsSource.Cast<Question>().OrderBy(t => t.Id);
            }
            else
            {
                questionList.ItemsSource = questionList.ItemsSource.Cast<Question>().OrderByDescending(t => t.Id);
            }
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(questionList.ItemsSource);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("Module");
            view.GroupDescriptions.Add(groupDescription);
        }
        private void btnSortByStatus_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isSortStatus = !isSortStatus;
            if (isSortStatus)
            {
                questionList.ItemsSource = questionList.ItemsSource.Cast<Question>().OrderByDescending(t => t.isAttempted);
            }
            else
            {
                questionList.ItemsSource = questionList.ItemsSource.Cast<Question>().OrderBy(t => t.isAttempted);
            }
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(questionList.ItemsSource);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("Module");
            view.GroupDescriptions.Add(groupDescription);
        }
        private void btnFinishAttempted_Click(object sender, RoutedEventArgs e)
        {
            SubmitedExamWindow sew = new SubmitedExamWindow();
            this.Close();
            sew.Show();
            CloseAllWindows();
        }
        private void CloseAllWindows()
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
            {
                if (intCounter != App.Current.Windows.Count - 1)
                {
                    App.Current.Windows[intCounter].Close();
                }
            }
        }
    }
}
