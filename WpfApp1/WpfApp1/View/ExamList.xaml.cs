﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for ExamList.xaml
    /// </summary>
    /// 
    public class Exam
    {
        public string Name { get; set; }
        public string Course { get; set; }
        public string ExamPeriod { get; set; }
        public string Duration { get; set; }
        public string? Status { get; set; }
        public string Pin { get; set; }
        public bool IsExpired { get; set; }
    }
    public partial class ExamList : Window
    {
        public const string uri = "https://localhost:7017/api/";
        public ExamList()
        {
            InitializeComponent();

            examList.ItemsSource = GetExams();

        }
        public List<Exam> GetExams()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                HttpResponseMessage response = httpClient.GetAsync(uri + "exam").Result;
                var exams = response.Content.ReadAsAsync<List<Exam>>().Result;
                return exams;
            }
        }
        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TextBlock textBlock = sender as TextBlock;
            var exam = textBlock?.DataContext as Exam;
            if (textBlock != null)
            {
                if (exam.Status == "Submited")
                {
                    SubmitedWindow smw = new SubmitedWindow(exam);
                    this.Close();
                    smw.Show();
                }
                else if (exam.Status == "InProgress")
                {
                    ExamPinWindow epw = new ExamPinWindow(exam);
                    epw.DataContext = exam;
                    this.Close();
                    epw.Show();
                }
            }

        }
    }
}
