﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for ExamPinWindow.xaml
    /// </summary>
    public partial class ExamPinWindow : Window
    {
        private Exam _exam;
        private bool isDsable = true;
        public ExamPinWindow()
        {
            InitializeComponent();
        }

        public ExamPinWindow(Exam passedExam) : this()
        {
            _exam = passedExam;
            this.DataContext = _exam;
        }

        public void BtnConfirm_Handler(object sender, RoutedEventArgs e)
        {
            if (txPin.Text.Length > 0 && isDsable == true)
            {
                isDsable = false;
                btnConfirm.Background = new SolidColorBrush(Color.FromRgb(27, 101, 248));
                btnConfirm.Foreground = new SolidColorBrush(Color.FromRgb(253, 253, 253));
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            ExamList elw = new ExamList();
            this.Close();
            elw.Show();
        }

        private async void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            isDsable = true;
            if (txPin.Text != _exam.Pin)
            {
                txblError.Visibility = Visibility.Visible;
                btnConfirm.Background = new SolidColorBrush(Color.FromRgb(194, 194, 194));
                btnConfirm.Foreground = new SolidColorBrush(Color.FromRgb(163, 163, 163));
            }
            else
            {
                txPin.IsEnabled = false;
                txblError.Visibility = Visibility.Collapsed;
                btnConfirm.Visibility = Visibility.Collapsed;
                btnBack.Visibility = Visibility.Collapsed;
                txblIniting.Visibility = Visibility.Visible;
                txblIniting.Text = "Initiating download...";
                pb.Visibility = Visibility.Visible;
                pb.Value = 20;
                int delayTime = 1000;
                await Task.Delay(delayTime);
                ExamGuideWindow egw = new ExamGuideWindow(_exam);
                this.Close();
                egw.Show();
            }
        }
    }
}
