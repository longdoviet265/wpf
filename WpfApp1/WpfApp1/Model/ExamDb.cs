﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Model
{
    public class ExamDb
    {
        public int Id { get; set; }
        public int ExamId { get; set; }
        public DateTime TimeStart { get; set; }
        public DateTime TimeFinish { get; set; }
        public DateTime EstimateEndTime { get; set; }
        public DateTime StartingTime { get; set; }
        public bool IsStart { get; set; }

    }
}
