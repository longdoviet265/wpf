﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.ViewModel;

namespace WpfApp1.Model
{
    public class Question : BaseViewModel
    {
        private int _id;
        private string _module;
        private int _type;
        private bool _isClick;
        private bool _isReview;
        private bool _isCurrent;
        private bool _isAttempted;
        public int Id
        {
            get => _id;
            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }
        public string Module
        {
            get => _module;
            set
            {
                _module = value;
                OnPropertyChanged();
            }
        }
        public int Type
        {
            get => _type;
            set
            {
                _type = value;
                OnPropertyChanged();
            }
        }
        public bool isClick
        {
            get => _isClick;
            set
            {
                _isClick = value;
                OnPropertyChanged();
            }
        }
        public bool isReview
        {
            get => _isReview;
            set
            {
                _isReview = value;
                OnPropertyChanged();
            }
        }

        public bool isCurrent
        {
            get => _isCurrent;
            set
            {
                _isCurrent = value;
                OnPropertyChanged();
            }
        }

        public bool isAttempted
        {
            get => _isAttempted;
            set
            {
                _isAttempted = value;
                OnPropertyChanged();
            }
        }
    }
}
