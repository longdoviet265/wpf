﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Db
{
    public interface ILocalDb
    {
        public SqlConnection GetDbConnection();
        public DataTable GetTable(string sqlText);
        public void ExcuteSql(string sqlText);
        public void CloseDbConnection();
    }
}
