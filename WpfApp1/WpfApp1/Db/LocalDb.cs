﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfApp1.Db
{
    public class LocalDb : ILocalDb
    {
        public SqlConnection GetDbConnection()
        {
            string connectionString = Properties.Settings.Default.Connection_String;
            SqlConnection cnn = new SqlConnection(connectionString);
            if (cnn.State != ConnectionState.Open) cnn.Open();
            return cnn;
        }
        public DataTable GetTable(string sqlText)
        {
            SqlConnection cnn = GetDbConnection();
            DataTable tb = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(sqlText, cnn);
            adapter.Fill(tb);
            return tb;
        }
        public void ExcuteSql(string sqlText)
        {
            SqlConnection cnn = GetDbConnection();
            SqlCommand cmd = new SqlCommand(sqlText, cnn);
            cmd.ExecuteNonQuery();
        }
        public void CloseDbConnection()
        {
            SqlConnection cnn = GetDbConnection();
            cnn.Close();
        }
    }
}
