﻿using System.Runtime.InteropServices;

namespace wpfAPi.Controllers.Model
{
    public class Exam
    {
        public string Name { get; set; }
        public string Course { get; set; }
        public string ExamPeriod { get; set; }
        public string Duration { get; set; }
        public string Status { get; set; }
        public string Pin { get; set; }
        public bool IsExpired { get; set; }
    }
}
