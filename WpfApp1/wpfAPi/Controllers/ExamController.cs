﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using wpfAPi.Controllers.Model;

namespace wpfAPi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExamController : ControllerBase
    {
        [NonAction]
        public List<Exam> ExamSeeder()
        {
            List<Exam> exams = new List<Exam>();
            exams.Add(new Exam() { Name = "Exam A", Course = "Course A", ExamPeriod = "May 26th 2023 - May 27th 2023", Duration = "60", Status = "Submited", Pin = "123" });
            exams.Add(new Exam() { Name = "Exam B", Course = "Course B", ExamPeriod = "Jun 16th 2023 - Jun 16th 2023", Duration = "50", Status = "InProgress", Pin = "123" });
            exams.Add(new Exam() { Name = "Exam C", Course = "Course C", ExamPeriod = "Mar 15th 2023 - Mar 15th 2023", Duration = "40", Status = null, Pin = "123" });
            exams.Add(new Exam() { Name = "Exam C", Course = "Course C", ExamPeriod = "Mar 15th 2023 - Mar 15th 2023", Duration = "40", Status = "InProgress", Pin = "123" });
            exams.Add(new Exam() { Name = "Exam D", Course = "Course D", ExamPeriod = "Mar 15th 2023 - Mar 15th 2023", Duration = "40", Status = null, Pin = "123" });
            exams.Add(new Exam() { Name = "Exam E", Course = "Course E", ExamPeriod = "Mar 15th 2023 - Mar 15th 2023", Duration = "40", Status = "Submited", Pin = "123", IsExpired = true });
            exams.Add(new Exam() { Name = "Exam F", Course = "Course F", ExamPeriod = "Mar 15th 2023 - Mar 15th 2023", Duration = "40", Status = null, Pin = "123" });
            exams.Add(new Exam() { Name = "Exam G", Course = "Course G", ExamPeriod = "Mar 15th 2023 - Mar 15th 2023", Duration = "40", Status = "InProgress", Pin = "123" });
            exams.Add(new Exam() { Name = "Exam H", Course = "Course H", ExamPeriod = "Mar 15th 2023 - Mar 15th 2023", Duration = "40", Status = "Submited", Pin = "123" });
            exams.Add(new Exam() { Name = "Exam I", Course = "Course I", ExamPeriod = "Mar 15th 2023 - Mar 15th 2023", Duration = "40", Status = null, Pin = "123" });
            exams.Add(new Exam() { Name = "Exam K", Course = "Course K", ExamPeriod = "Mar 15th 2023 - Mar 15th 2023", Duration = "40", Status = "InProgress", Pin = "123" });
            exams.Add(new Exam() { Name = "Exam M", Course = "Course M", ExamPeriod = "Mar 15th 2023 - Mar 15th 2023", Duration = "40", Status = null, Pin = "123" });
            exams.Add(new Exam() { Name = "Exam N", Course = "Course N", ExamPeriod = "Mar 15th 2023 - Mar 15th 2023", Duration = "40", Status = "InProgress", Pin = "123" });
            exams.Add(new Exam() { Name = "Exam L", Course = "Course L", ExamPeriod = "Mar 15th 2023 - Mar 15th 2023", Duration = "40", Status = "Submited", Pin = "123" });
            exams.Add(new Exam() { Name = "Exam O", Course = "Course O", ExamPeriod = "Mar 15th 2023 - Mar 15th 2023", Duration = "40", Status = null, Pin = "123" });
            exams.Add(new Exam() { Name = "Exam P", Course = "Course P", ExamPeriod = "Mar 15th 2023 - Mar 15th 2023", Duration = "40", Status = "Submited", Pin = "123" });
            return exams;
        }

        [HttpGet]
        public async Task<ActionResult<List<Exam>>> Get()
        {
            List<Exam> exams = ExamSeeder();
            return Ok(exams);
        }
    }
}
